# ELC Coding Test

The ELC Coding Test is a way for you to showcase your own approach to coding. It allows you to create something with your own style and preferences. You can change the code to match your own preferences however you like. Feel free to change the setup, code or approach however you like.

### The Task

You are required to create a simple auto-search feature similar to [this one](https://www.maccosmetics.co.uk/) where-by as you type the data is checked against a Node Server and the response is then loaded in with React. You can style this however you wish and can implement it however you wish but you *must* use React and Node to accomplish this. This has already been setup for you and can be found in the files here. Some general tips on starting:

* The point of entry for the app can be found in the 'app/scripts/main.js' file - work from there for your React code.
* The SCSS files contain the SASS styling and is the place to put all your SCSS
* You can alter the setup of the runtime by using the Gulpfile.js
* The server with the response can be found in the 'server' folder
* The data can be found in the 'server' folder in the data.js file which is then loaded into the node 'server/app.js' file

## Getting Started

### Prerequisites

* NodeJS (8.0+)

The first thing you need to do to get this app working is to grab the source code. Once you have the source code you must install the node modules with the following command:

    npm install

From here you should be good to start the servers.

## Running the Server

To get the app working you will need to run two locally hosted servers. The first one is the Back End Server which runs the NodeJS files and the second is Front End server which runs a ReactJS app.

### Step 1

The first step is to start the Back End NodeJS server. The files for this can be found in the './server/' folder. To start this server run it with the following in the terminal from the root directory:

    npm run node-server

This will start the NodeJS server with Nodemon so that updates happen automatically on save.

On Windows systems you may get an error like the following:
  'NODE_ENV' is not recognized as an internal or external command, operable program or batch file.
If so you will need to replace any occurrences of NODE_ENV with 'SET NODE_ENV' in package.json, and separate this command from the one that follows it with a '&', like this:
  "node-server": "SET NODE_ENV=development & nodemon server/app.js",

### Step 2

To run the HTML server simple call one of these commands:

    npm run dev-server

This will create a server that runs the code found in the App Folder. This folder is split into a few separate folders.

It should also launch your web browser and load the local dev server address. A page should display showing a basic web UI with a top nav and search. If this does not appear, it may be necessary to update the Babel version as some modules have a dependency on beta 41 instead of 34.

# Folders

The folders for all the Front End source code can be found in the './app' folder.

## images

Here you can place images that can be processed with the npm command:

    npm run image-min

This will minify the images and put them into the '.local_server/img/' folder.

## sass

Here you will find the SCSS files, we use Sass on the project to compile down to CSS. These files will be automatically compiled upon save when you are running the standard 'npm run dev-server' command. However you can compile this yourself by running this command:

    npm run sass

## scripts

All of the Javascript can be found in here. The App uses React so all of these files are written with React JSX and compiled via Webpack. The Webpack setup can be found in the 'config/webpack.config.js' file. These files are ran with the '[@babel/preset-env](https://github.com/babel/babel/tree/master/packages/babel-preset-env)' and '[@babel/preset-react](https://www.npmjs.com/package/@babel/preset-react)' loaders, which will enable you to write ES2017 and React Code.

The code is all initialised from the 'app/scripts/main.js' file, so that should be your initial point of call for the App.

## third_party

Third party can be used to contain any third party libraries that you may want to use with your app. You can call a command to move all the third party items with:

    npm run third-party

## views

The views folder contains the HTML templates folder. The templates are created with the [Mustache](https://mustache.github.io/) templating language.


# Node.JS Back End Server

To start the API on its own you can run this command:

    npm run dev-node-server

The data found in the NodeJS server will use the following JSON Schema:

    {
        "_id": "001", // A Number for the product
        "isActive": "false", // Is the product actively in stock
        "price": "23.00", // The price of the product in the set currency
        "picture": "/img/products/N16501_430.png", // The location of the image for the product
        "name": "Damage Reverse Thickening Conditioner", // The products name
        "about": "Description for the product...", // Description of the product
        "tags": [ "ojon", "conditioner" ] // The tags for the product
    }
