/**
 * This file will hold the Menu that lives at the top of the Page, this is all rendered using a React Component...
 *
 */
import React from 'react';
import Autosuggest from 'react-autosuggest';

var data;

// Teach Autosuggest how to calculate suggestions for any given input value.
const getSuggestions = value => {
  const inputValue = value.trim().toLowerCase();
  const inputLength = inputValue.length;

  return inputLength === 0 ? [] : data.filter(lang =>
    lang.name.toLowerCase().slice(0, inputLength) === inputValue
  );
};

/**
 * When suggestion is clicked, Autosuggest needs to populate the input
 * based on the clicked suggestion. Teach Autosuggest how to calculate the
 * input value for every given suggestion.
 * @param suggestion
 * @returns {*}
 */
const getSuggestionValue = suggestion => suggestion.name;

  /*
  * Use your imagination to render suggestions.
  */
const renderSuggestion = suggestion => (
  <div>
    {suggestion.name}
  </div>
);


class Menu extends React.Component {

  /**
   * Main constructor for the Menu Class
   * @memberof Menu
   */
  constructor() {
    super();
    this.state = {
      showingSearch: false,
      value: '',
      suggestions: [],
      data: [],
    };

    const url = 'http://localhost:3035/search';
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
    })
      .then((a) => {
        return a.json(); // call the json method on the response to get JSON
      })
      .then((json) => {
        console.log(json);
        data = json;
      });
  }

  /**
   * On change
   * @param event
   * @param newValue
   */
  onChange(event, {newValue}) {
    this.setState({
      value: newValue
    });
  };

  /**
   * Autosuggest will call this function every time you need to update suggestions.
   * You already implemented this logic above, so just use it.
   */
  onSuggestionsFetchRequested({value}) {
    this.setState({
      suggestions: getSuggestions(value)
    });
  };

  /**
   * Autosuggest will call this function every time you need to clear suggestions.
   */
  onSuggestionsClearRequested() {
    this.setState({
      suggestions: []
    });
  };

  /**
   * Shows or hides the search container
   * @memberof Menu
   * @param e [Object] - the event from a click handler
   */
  showSearchContainer(e) {
    e.preventDefault();
    this.setState({
      showingSearch: !this.state.showingSearch
    });
  }

  /**
   * Renders the default app in the window, we have assigned this to an element called root.
   *
   * @returns JSX
   * @memberof App
   */
  render() {
    const {value, suggestions} = this.state;

    // Autosuggest will pass through all these props to the input.
    const inputProps = {
      placeholder: 'Search',
      value,
      onChange: this.onChange.bind(this)
    };
    return (
      <header className="menu">
        <div className="menu-container">
          <div className="menu-holder">
            <h1>ELC</h1>
            <nav>
              <a href="#" className="nav-item">HOLIDAY</a>
              <a href="#" className="nav-item">WHAT'S NEW</a>
              <a href="#" className="nav-item">PRODUCTS</a>
              <a href="#" className="nav-item">BESTSELLERS</a>
              <a href="#" className="nav-item">GOODBYES</a>
              <a href="#" className="nav-item">STORES</a>
              <a href="#" className="nav-item">INSPIRATION</a>

              <a href="#" onClick={(e) => this.showSearchContainer(e)}>
                <i className="material-icons search">search</i>
              </a>
            </nav>
          </div>
        </div>
        <div className={(this.state.showingSearch ? "showing " : "") + "search-container"}>
          <a href="#" onClick={(e) => this.showSearchContainer(e)}>
            <i className="material-icons close">close</i>
          </a>
          <Autosuggest
            suggestions={suggestions}
            onSuggestionsFetchRequested={this.onSuggestionsFetchRequested.bind(this)}
            onSuggestionsClearRequested={this.onSuggestionsClearRequested.bind(this)}
            getSuggestionValue={getSuggestionValue}
            renderSuggestion={renderSuggestion}
            inputProps={inputProps}
          />
        </div>
      </header>
    );
  }
}

// Export out the React Component
module.exports = Menu;